<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();
class AdminController extends Controller
{
   public function index(){


      return view('admin_login');
   }

   public function show_dashboard(){


      return view('admin.dashboard');
   }

   public function dashboard(Request $request){

      $admin_email=$request->admin_email;
      $admin_password=$request->admin_password;
      $result=DB::table('tbl_admin')
          ->where('admin_email',$admin_email)
          ->where('admin_password',$admin_password)
          ->first();
         if($result) {
            Session::put('admin_name', $request->admin_name);
            Session::put('admin_id', $request->admin_id);
            return Redirect::to('/dashboard');
         }else{
             Session::put('massage','Email or Password Invalid');
            return Redirect::to('/admin');

         }
   }
}

